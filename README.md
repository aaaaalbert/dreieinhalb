# dreieinhalb

Material zum 3,5-GHz-Band

# Händler_innen

* https://www.triotronik.com/de/SHOP/Wireless---0.7.html
* https://shop.meconet.de/

# 802.11y und Konsorten

aka "WLAN auf 3,5 GHz"

| Hersteller_in | Gerät       | Typ   | GHz       | dBm | Euro | Kommentar | Link
| --------- | --------------- | ----- | --------- | --- | ---- | --------- | ------ |
| Ubiquiti | Rocket M3        | BS    | 3,37-3,73 | 25  |  170 |  | https://dl.ubnt.com/datasheets/rocketm/RocketM_DS.pdf
| Ubiquiti | NanoStation NSM3 | Kombi | 3,40-3,70 | 25  |  130 |  | https://dl.ubnt.com/datasheets/nanostationm/nsm_ds_web.pdf
| Ubiquiti | NanoBridge M3    | Kombi | 3,37-3,73 | 25  |   60 | nur 54 Mbps!? | https://dl.ubnt.com/datasheets/nanobridgem/nbm_ds_web.pdf
| Ubiquiti | airFiber AF-3X   | BS    | 3,30-3,90 | 29  |  750 |  | https://dl.ubnt.com/datasheets/airfiber/airFiber_X_DS.pdf
| Ubiquiti | AF-3G26-S45    | Antenne | 3,30-3,80 | +26 |  260 | 650mm | https://dl.ubnt.com/datasheets/airfiber/airFiber_X_DS.pdf

